# Forcing ipv4
echo 'Acquire::ForceIPv4 "true";' | sudo tee /etc/apt/apt.conf.d/99force-ipv4
# Installing Puppet
sudo apt update
sudo apt install -y openjdk-8-jdk
sudo apt install -y puppet
# Installing Sonar
sudo puppet module install fraenki-sonarqube
wget https://gitlab.com/roybhaskar9/devops/raw/master/release/jenkins/jenkinsserver/sonar.pp
sudo puppet apply sonar.pp
sudo echo "sonar.embeddedDatabase.port:               9092" >> /usr/local/sonar/conf/sonar.properties
sudo /etc/init.d/sonar start
# Installing Maven
sudo apt-get install -y maven
sudo rm -f /etc/maven/settings.xml
sudo wget https://raw.githubusercontent.com/roybhaskar9/samplejava/master/settings.xml -O /etc/maven/settings.xml
# Installing Jenkins
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
wget http://pkg.jenkins-ci.org/debian/binary/jenkins_2.346_all.deb
sudo apt-get install -y ./jenkins_2.346_all.deb
# installing Selenium
curl -sSL https://gitlab.com/roybhaskar9/devops/-/raw/master/release/jenkins/jenkinsserver/selenium.sh |bash
# Installing Nexus
# Do it Manually https://codbucket.com/install-nexus-repository-on-ubuntu-step-by-step-guide/
echo "Setup complete except Nexus"
echo "Set up Nexus manually following tutorial at https://codbucket.com/install-nexus-repository-on-ubuntu-step-by-step-guide/"
