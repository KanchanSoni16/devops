#! /bin/bash
git clone https://github.com/roybhaskar9/vagrant-kubeadm-kubernetes
cd vagrant-kubeadm-kubernetes
echo "do you want to review settings.yaml [Y,n]?"
read yamlread
if [[ $yamlread == "Y" || $yamlread == "y" ]]; then
        cat settings.yaml
fi
echo "do you want to edit that [Y,n]?"
read editthat
if [[ $editthat == "Y" || $editthat == "y" ]]; then
        nano settings.yaml
else
        vagrant up
        vagrant ssh master
fi
